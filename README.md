# managesocieties

#### 介绍
{**以下是 Gitee 平台说明，您可以替换此简介**
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

## request封装请求使用
### 在所需*.vue页面
```js
this.$request({
  method: 'POST', // 默认GET
  url: '',
  data: data,
  header: {}, // 可不填，虽然多半不可能
}).then(res => {
  // 成功后执行，具体格式待对完接口完善
}).catch(res => {
  // 失败后执行
})
### 
```
### 二轮接口
1.个人投递记录获取
get
form[
	{
		orgName:'',
		id:'',//简历id,
	}
]

3.两个简历基本信息获取分别为组织获取与个人获取
post
发送数据：
组织token/个人token（两个接口）
返回数据
list: [
		{
			id: 1,//简历id
			name:'',
			store:'//学号',
			qq:'//QQ号',		
		},
	],
4.简历ID获取简历所有信息
这里看后端怎么做，即所以简历放在一起还是单独分出来，前端方便或者更改简历获取，可以将两个数据作为请求参数，分别为简历id与简历类别（社团，组织）
get
发送数据
id:''
返回数据
社团：
model: {
					name: '',
					sex: '',//性别
					colleg:'',//学院
					specialize:'',//专业
					study_store:'',	//学号			
					polity_mask:'',//政治面貌
					qq:'',//qq
					phone:'',//电话
					societies_num_1:'',
					societies_name_1:'',
					societies_num_2:'',
					societies_name_2:'',
					photo: ''
				},
组织：
model: {
					name: '',
					sex: '',//性别
					colleg:'',//学院
					specialize:'',//专业
					grade:'',
					study_store:'',	//学号			
					polity_mask:'',//政治面貌
					qq:'',//qq
					phone:'',//电话
					org:'',//组织
					room:'',//部门
					reason:'',//理由
					selfEvaluation:'',//自我评价
					experience:'',//经历
					photo:''
				},

2.组织登录
post
发送数据：
form{
	number:'',//账号
	password:''//密码
}判断有无即可
返回数据:
账号密码错误/登录成功
data{
	token:'',
	class:''组织类别 到时候自己存类别
}

3.组织分类
点击下拉分类发送两个类别数据
发送数据
form:{
	class_one:'',(社团，院级，校级)
	class_two:''（物种分类）
}
返回数据
返回值同为organization/allorgmsg接口数据相同
```js
data:{
      "index_img": "ad do tempor reprehenderit",
      "name": "consectetur in ut est Duis",
      "rId": "consectetur laboris fugiat ipsum",
      "class_one": "sunt velit",
      "class_two": "ad sed ex",
      "hot": "tempor",
      "intro": "Duis irure magna qui voluptate"
    },
	```
5.获取所有活动
get
```js
data:{
      "index_img": "ad do tempor reprehenderit",//活动封面
      "name": "consectetur in ut est Duis",//活动名称
      "rId": "consectetur laboris fugiat ipsum",//活动id
      "class_one": "sunt velit",
      "class_two": "ad sed ex",
      "hot": "tempor",
      "intro": "Duis irure magna qui voluptate"//活动内容
    },
	```
4.活动分类
点击下拉分类发送两个类别数据
发送数据
```js
form:{
	class_one:'',(社团，院级，校级)
	class_two:''（物种分类）
}
```
返回数据
```js
data:{
      "index_img": "ad do tempor reprehenderit",//活动封面
      "name": "consectetur in ut est Duis",//活动名称
      "rId": "consectetur laboris fugiat ipsum",//活动id
      "class_one": "sunt velit",
      "class_two": "ad sed ex",
      "hot": "tempor",
      "intro": "Duis irure magna qui voluptate"//活动内容
    },
```
5.活动详细信息获取
get
发送数据
id:''//活动id
返回数据
```js
data:{
      index_img: "ad do tempor reprehenderit",//活动封面
      name: "consectetur in ut est Duis",//活动名称
      rId: "consectetur laboris fugiat ipsum",//活动id
      video:"",//活动视频
      rules:"",//活动规则
      files:[
      		  '',
      		  ''],//活动附件
      act_img:''//活动群聊
       intro: "Duis irure magna qui voluptate",//活动内容，
      class_one: "sunt velit",
      class_two: "ad sed ex",
      hot: "tempor",
    },
```

6.活动发布
```js
data:{
      index_img: "ad do tempor reprehenderit",//活动封面
      name: "consectetur in ut est Duis",//活动名称
      rId: "consectetur laboris fugiat ipsum",//活动id
	  video:"",//活动视频
	  rules:"",//活动规则
	  files:[
		  '',
		  ''],//活动附件
	  act_img:''//活动群聊
	   intro: "Duis irure magna qui voluptate",//活动内容，
      class_one: "sunt velit",
      class_two: "ad sed ex",
      hot: "tempor",//热度
    },
```

7.招新发布
```js
inform:{
					departments:[
						'办公室：10人',
						'宣传部：10人'
					],
					timeaddr:[{
						time:'2021-10-10',addr:'c区'
					},{
						time:'2021-10-10',addr:'c区'
					}],
					introduce:'这是一个段落，这是一个段落，这是一个段落，这是一个段落，这是一个段落，这是一个段落，这是一个段落，这是一个段落，这是一个段落，这是一个段落，',
					qrcode:'https://cdn.uviewui.com/uview/swiper/3.jpg',
					video:'1',
					oId:'1',
					img:'https://cdn.uviewui.com/uview/swiper/3.jpg',
					title:'',
					class_one:'',
					class_two:''
				},
```
				
8.社团招新发布
社团名称，社团简介，社团视频，社团编号，社团封面，社团视频，社团活动图片
```js
data:{
	name:'',
	intro:'',
	video:'',
	number:'',
	index_img:'',
	stImg:[
		'',
		''
	]
}
```

9.管理页，各次组织招新人数与活动报名人数获取
get
发送数据：组织token/组织ID
返回数据：
	orgs:[
			{
				id:'',//招新id用于获取招新的所有学生基本信息
				number:''//招新报名数量
			}
		],
	act:[
			{
				id:'',//招新id用于获取活动的所有学生基本
				number:''//活动报名数量
			}
		]
10：获得文件压缩包
即导出所有招新个人文件
get
发送数据：
活动/招新ID
返回数据：
file:'',//压缩包
### 接口更改
1.一轮接口简历提交后新增返回数据：简历生成的文件
### 功能优化（待做）
 1.保存草稿功能
2.草稿查看功能

---
## 正式二轮接口
### 社团报名

**请求URL**

/*** `POST`

**请求头**

```json
"headers": {
  "Authorization": "Bearer" + token
}
```

**请求参数**

参数名||类型|必须|描述
:--:|:--:|:--:|:--:|:--:
model||object|是|报名表单
||||
/|name|string|是|
/|sex|string|是|性别
/|colleg|string|是|学院
/|specialize|string|是|专业
/|study_store|string|是|学号			
/|polity_mask|string|是|政治面貌
/|qq|string|是|qq
/|phone|string|是|电话
/|societies_num_1|string|是|
/|societies_name_1|string|是|
/|societies_num_2|string|是|
/|societies_name_2|string|是|
/|photo|string|是|照片

**返回数据**

```
file:''
```
### 更改组织报名文件

**请求URL**

/*** `POST`

**请求头**

```json
"headers": {
  "Authorization": "Bearer" + token
}
```

**请求参数**

参数名||类型|必须|描述
:--:|:--:|:--:|:--:|:--:
model||object|是|报名表单
||||
/|name|string|是|
/|sex|string|是|性别
/|colleg|string|是|学院
/|specialize|string|是|专业
/|grade|string|是|
/|study_store|string|是|	学号			
/|polity_mask|string|是|政治面貌
/|qq|string|是|qq
/|phone|string|是|电话
/|org|string|是|组织
/|room|string|是|部门
/|reason|string|是|理由
/|selfEvaluation|string|是|自我评价
/|experience|string|是|经历
/|photo|string|是|照片

**返回结果**

```
files:'',//组织报名解析返回文件
```
### 组织分类

>点击下拉分类发送两个类别数据

**请求URL**

/*** `GET`

**请求参数**
参数名||类型|必须|描述
:--:|:--:|:--:|:--:|:--:
form||object|是|
||||
/|class_one|string|社团，院级，校级
/|class_two|string|物种分类

**返回结果**

```
  返回值同为organization/allorgmsg接口数据相同
```
### 组织B端登录

**请求URL**

/*** `POST`

**请求头**

```json
"headers": {
  "Authorization": "Bearer" + token
}
```

**请求参数**

参数名|类型|必须|描述
:--:|:--:|:--:|:--:
account|string|是|账号
password|string|是|密码

**返回结果**

```json
{
  "token":"",
  "class_one":"//组织类别"
}
```
### 获取历次招新的报名人数

**请求URL**

/*** `GET`

**请求头**

```json
"headers": {
  "Authorization": "Bearer" + token
}
```

**返回结果**
```json
{
  "oId":"//组织id——存于缓存，用于查询相关信息",
  "orgs":[
    {
      "id":"//招新id用于获取招新的所有学生基本(6接口)",
      "number":"//招新报名数量"
    }
  ]
}
```
### 某次招新ID获取招新学生基本信息

**请求URL**

/*** `GET`

**请求参数**

参数名|类型|必须|描述
:--:|:--:|:--:|:--:
id|string|是|

**返回结果**
```json
{
  "list": [
    {
      "id": "1 //简历id",
      "name": "//姓名",
      "store":"//学号",
      "qq":"//QQ号"
      
    },
    {
      "id": "2 //简历id",
      "name": "//姓名",
      "store": "//学号",
      "qq": "//QQ号"
    },
  ],
}
```
### 简历id查询信息

>判断是否为组织/社团请求不同的接口

**请求参数**

参数名|类型|必须|描述
:--:|:--:|:--:|:--:
id|string|是|

**返回结果**
```json
//社团简历接口返回数据
"model": {
  "name": "",
  "sex": "//性别",
  "colleg": "//学院",
  "specialize": "//专业",
  "study_store": "//学号",			
  "polity_mask": "//政治面貌",
  "qq": "//qq",
  "phone": "//电话",
  "societies_num_1": "",
  "societies_name_1": "",
  "societies_num_2": "",
  "societies_name_2": "",
  "photo": ""
},
//组织简历返回数据
"model": {
  "name": "",
  "sex": "//性别",
  "colleg": "//学院",
  "specialize": "//专业",
  "grade": "",
  "study_store": "//学号",			
  "polity_mask": "//政治面貌",
  "qq": "//qq",
  "phone": "//电话",
  "org": "//组织",
  "room": "//部门",
  "reason": "//理由",
  "selfEvaluation": "//自我评价",
  "experience": "//经历",
  "photo": ""
}
```
### 社团招新发布

**请求URL**

/*** `POST`

**请求头**

```json
"headers": {
  "Authorization": "Bearer" + token
}
```

**请求参数**

参数名||类型|必须|描述
:--:|:--:|:--:|:--:|:--:
inform||object|是|
||||
/|name|string|是|//社团名称，
/|intro|string|是|//社团简介，
/|video|string|是|//社团视频，
/|number|string|是|//社团编号
/|imgs|array|是|//社团活动图片
/|index_img|string|是|//社团封面				
token:''

**返回结果**
```json
{
  "msg": "//发布成功或者发布失败"
}
```
### 社团招新信息获取

**请求URL**

/*** `GET`

**请求参数**

参数名|类型|必须|描述
:--:|:--:|:--:|:--:
id|string|是|//社团id

**返回结果**
```json
"inform": {
  "name": "//社团名称",
  "intro": "//社团简介",
  "video": "//社团视频",
  "number": "//社团编号",
  "imgs": "[] //社团活动图片",
  "index_img": "//社团封面",
}
```
