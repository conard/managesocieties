const baseUrl = 'https://hd.jxj777.top'

export function request({ method = 'get', url, data, header }) {
  return new Promise((resolve, reject) => {
    uni.request({
      method: method,
      url: baseUrl + url,
      data: data || '',
      header: header || '',
      dataType: 'json',
    }).then((response) => {
      setTimeout(function() {
        uni.hideLoading();
      }, 200);
      response = response || '';
      let [error, res, reason] = response;
      resolve(res.data);
    }).catch((error) => {
      error = error || '';
      let [err, res, reason] = error;
      reject(err)
    })
  });
}
export function uploadFile({ url, name, filePath, data, header }) {
  return new Promise((resolve, reject) => {
    uni.uploadFile({
      url: baseUrl + url,
      name: name,
      filePath: filePath,
      formData: data || '',
      header: header || '',
      // dataType: 'json',
    }).then((response) => {
      setTimeout(function() {
        uni.hideLoading();
      }, 200);
      response = response || '';
      let [error, res, reason] = response;
      resolve(res.data);
    }).catch((error) => {
      error = error || '';
      let [err, res, reason] = error;
      reject(err)
    })
  });
}
// hd.jxj777.top:8081
// http://qlapi.sylu.edu.cn/mock/269
